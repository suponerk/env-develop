#!/bin/sh
set -e

# libre office
sudo add-apt-repository ppa:libreoffice/ppa
sudo apt install -y libreoffice --no-install-recommends --no-install-suggests

# keepass
sudo add apt install -y keepass2 --no-install-recommends --no-install-suggests

# openconnect with gui in ubuntu
sudo add apt install -y openconnect --no-install-recommends --no-install-suggests
sudo add apt install -y network-manager-openconnect-gnome --no-install-recommends --no-install-suggests
# then add this connection on top-right corner: Network, VPN, click +, add config
# then in config enable checkbox: use only for internal 

# docker-ce
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - # Add official GPG key    
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
sudo apt install -y docker-ce --no-install-recommends --no-install-suggests

# git
sudo apt install -y git --no-install-recommends --no-install-suggests
git config --global pull.rebase true
git config --global user.name "Andrey Zakharov"
git config --global user.email ""

# vscode
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt update
sudo apt install -y code

# vscode-extensions
code --install-extension --force mhutchie.git-graph
code --install-extension --force rangav.vscode-thunder-client
code --install-extension --force vscjava.vscode-java-pack
code --install-extension --force gabrielbb.vscode-lombok
code --install-extension --force rangav.vscode-thunder-client