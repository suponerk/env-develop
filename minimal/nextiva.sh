#!/bin/sh
set -e

# maven precondition
echo "\
<?xml version="1.0" encoding="UTF-8"?>
<settings>
    <mirrors>
        <mirror>
            <!-- This sends everything else to maven group repository -->
            <id>nexus</id>
            <mirrorOf>*</mirrorOf>
            <url>http://repository.nextiva.xyz:8081/nexus/content/repositories/public</url>
        </mirror>
    </mirrors>
    <profiles>
        <profile>
            <id>nexus</id>
            <!-- Send Nexus artifact requests through mirror -->
            <repositories>
                <repository>
                     <id>central</id>
                    <url>http://central</url>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                     <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                 </repository>
             </repositories>
            <pluginRepositories>
                 <pluginRepository>
                    <id>central</id>
                     <url>http://central</url>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                    </snapshots>
                </pluginRepository>
             </pluginRepositories>
         </profile>
    </profiles>
    <activeProfiles>
        <!--make the profile active all the time -->
        <activeProfile>nexus</activeProfile>
    </activeProfiles>
</settings>
" > $HOME/.sdkman/candidates/maven/current/conf/settings.xml